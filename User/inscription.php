<!-- session_start();  Prévenir php que l'on va utilisé les sessions (toujours utlisé en haut de page) -->

<?php

	session_start();

	if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_two'])){

			// variables 

			$email           = htmlspecialclhars($_POST['email']);
			$password        = htmlspecialclhars($_POST['password']);
			$password_two    = htmlspecialclhars($_POST['password-two']);

			// Password = Password two

			if($password != $password_two){

				header('location: inscription.php?error=1&message=Vos mots de passe ne sont pas identiques.');
				exit();
			
			}

	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Â M E L O T U S</title>
	<link rel="stylesheet" type="text/css" href="design/default.css">
	<link rel="icon" type="image/pngn" href="img/favicon.png">
</head>
<body>

<h1 class="text-logo"><img class="logo" src="images/logo.png"> Â M E L O T U S </h1>

	<?php include('src/header.php'); ?>
	
	<section>
		<div id="login-body">
			<h1>S'inscrire</h1>

		<?php if(isset($_GET['error'])){

			if(isset($_GET['message'])){

				echo'<div>'.htmlspecialchars($_GET['message']).'</div>';
			}
		}
		?>

			<form method="post" action="inscription.php">
				<input type="email" name="email" placeholder="Votre adresse email" required />
				<input type="password" name="password" placeholder="Mot de passe" required />
				<input type="password" name="password_two" placeholder="Retapez votre mot de passe" required />
				<button type="submit">S'inscrire</button>
			</form>

			<p class="grey">Déjà sur ÂMELOTUS ? <a href="index.php">Connectez-vous</a>.</p>
		</div>
	</section>

	<?php include('src/footer.php'); ?>
</body>
</html>